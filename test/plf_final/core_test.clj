(ns plf-final.core-test
  (:require [clojure.test :refer :all]
            [plf-final.core :refer :all]))

(deftest regresa-al-punto-de-origen?-test
  (testing "Dada una secuencia de sentidos indicar si se regresa al punto de origen:"
    (is (= true (regresa-al-punto-de-origen? "")))
    (is (= true (regresa-al-punto-de-origen? [])))
    (is (= true (regresa-al-punto-de-origen? (list))))

    (is (= true (regresa-al-punto-de-origen? "><")))
    (is (= true (regresa-al-punto-de-origen? (list \> \<))))
    (is (= true (regresa-al-punto-de-origen? "v^")))
    (is (= true (regresa-al-punto-de-origen? [\v \^])))
    (is (= true (regresa-al-punto-de-origen? "^>v<")))
    (is (= true (regresa-al-punto-de-origen? (list \^ \> \v \<))))
    (is (= true (regresa-al-punto-de-origen? "<<vv>>^^")))
    (is (= true (regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])))

    (is (= false (regresa-al-punto-de-origen? ">")))
    (is (= false (regresa-al-punto-de-origen? (list \>))))
    (is (= false (regresa-al-punto-de-origen? "<^")))
    (is (= false (regresa-al-punto-de-origen? [\< \^])))
    (is (= false (regresa-al-punto-de-origen? ">>><<")))
    (is (= false (regresa-al-punto-de-origen? (list \> \> \> \< \<))))
    (is (= false (regresa-al-punto-de-origen? [\v \v \^ \^ \^])))))

(deftest regresan-al-punto-de-origen?-test
  (testing "Dada n secuencias de sentidos indicar si todas regresan a su propio punto de origen:"

    (is (= true (regresan-al-punto-de-origen? (vector []))))
    (is (= true (regresan-al-punto-de-origen? (vector ""))))
    (is (= true (regresan-al-punto-de-origen? (vector [] "" (list)))))
    (is (= true (regresan-al-punto-de-origen? (vector "" "" "" "" [] [] [] (list) ""))))
    (is (= true (regresan-al-punto-de-origen? (vector ">><<" [\< \< \> \>] (list \^ \^ \v \v)))))

    (is (= false (regresan-al-punto-de-origen? (vector (list \< \>) "^^" [\> \<]))))
    (is (= false (regresan-al-punto-de-origen? (vector ">>>" "^vv^" "<<>>"))))
    (is (= false (regresan-al-punto-de-origen? (vector [\< \< \> \> \> \> \> \> \> \>]))))))


(deftest regreso-al-punto-de-origen?-test
  (testing "Dada una secuencia de sentidos, regresar la secuencia de sentidos que permita regresar al punto de origen en sentido contrario:"

    (is (= (list) (regreso-al-punto-de-origen "")))
    (is (= (list) (regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))
    (is (= (list \< \< \<) (regreso-al-punto-de-origen ">>>")))
    (is (= (list \< \< \^ \^ \^ \>) (regreso-al-punto-de-origen [\< \v \v \v \> \>])))))

 (deftest mismo-punto-final?-test
   (testing "Dadas 2 secuencias de sentidos, las cuales parten del mismo punto de origen, indicar si ambas terminan en el mismo punto final:"

     (is (= true (mismo-punto-final? "" [])))
     (is (= true (mismo-punto-final? "^^^" "<^^^>")))
     (is (= true (mismo-punto-final? [\< \< \< \>] (list \< \<))))
     (is (= true (mismo-punto-final? (list \< \v \>) (list \> \v \<))))

     (is (= false (mismo-punto-final? "" "<")))
     (is (= false (mismo-punto-final? [\> \>] "<>")))
     (is (= false (mismo-punto-final? [\> \> \>] [\> \> \> \>])))
     (is (= false (mismo-punto-final? (list) (list \^))))))

(run-tests)

    

    