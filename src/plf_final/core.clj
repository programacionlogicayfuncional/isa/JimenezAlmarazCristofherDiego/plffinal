(ns plf-final.core)

(defn norte
  [xs]
  (count (apply vector (filter #(= % \^) (apply vector xs)))))

(defn este
  [xs]
  (count (apply vector (filter #(= % \>) (apply vector xs)))))

(defn oeste
  [xs]
  (count (apply vector (filter #(= % \<) (apply vector xs)))))

(defn sur
  [xs]
  (count (apply vector (filter #(= % \v) (apply vector xs)))))

(defn resultado
  [xs]
  (- (+ (* 2 (norte xs)) (este xs)) (+ (* 2 (sur xs)) (oeste xs))))

(defn regresa-al-punto-de-origen?
  [xs] 
  (zero? (resultado xs)))


(defn regresan-al-punto-de-origen?
  [& args]
  (zero? (resultado args)))

(defn regreso-al-punto-de-origen
  [xs]
  (if (zero? (resultado xs)) (list)
      (concat (repeat (este xs) \<)
              (repeat (norte xs) \v)
              (repeat (sur xs) \^)
              (repeat (oeste xs) \>))))

(defn mismo-punto-final?
  [xs ys]
  (== (resultado xs) (resultado ys)))

(defn coincidencias 
  [xs ys]
  )



(regresa-al-punto-de-origen? "")
(regresa-al-punto-de-origen? [])
(regresa-al-punto-de-origen? (list))
(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])

(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])

(regresan-al-punto-de-origen?)
(regresan-al-punto-de-origen? [])
(regresan-al-punto-de-origen? "")
(regresan-al-punto-de-origen? [] "" (list))
(regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")
(regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))

(regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])
(regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")
(regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])

(regreso-al-punto-de-origen "")
(regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))

(regreso-al-punto-de-origen ">>>") 
(regreso-al-punto-de-origen [\< \v \v \v \> \>])

(mismo-punto-final? "" [])
(mismo-punto-final? "^^^" "<^^^>")
(mismo-punto-final? [\< \< \< \>] (list \< \<))
(mismo-punto-final? (list \< \v \>) (list \> \v \<))

(mismo-punto-final? "" "<")
(mismo-punto-final? [\> \>] "<>")
(mismo-punto-final? [\> \> \>] [\> \> \> \>])
(mismo-punto-final? (list) (list \^))
